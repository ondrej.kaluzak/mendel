(


~fallbackAttack = 0.001;
~fallbackDecay = 120;
~fallbackRelease = 30;
~fallBackEnv = Env.new(levels: [0, 1, 1, 0], times: [~fallbackAttack, ~fallbackDecay, ~fallbackRelease], curve: [1, 0, -2]);

SynthDef(\cleanSine, {
	var sig, env, mainEnv, noize,  fallbackEnv;
	var finalAmp = 0.05;

	var mainAttack = 0.005;
	var mainRelease = 2;
	var grained;
	var cc = \cc.kr(0).linlin(0,127,0,2pi);
	var dcc = \dcc.kr(0).linlin(0,255,0,2pi);
	var impFreq = cc.cos.range(1,20);
	var fxBus =\fxBus.ir(0);
	var dirBus = In.kr(\dirBus.kr(0), 1);
	var trig = InTrig.kr(\trigBus.kr(0), 1);
	var multi = Trig.kr(trig, 0.1);
	//var freq = \freq.kr(100);

	var dirIndex = dirBus.linlin(0,127,0,2pi).cos.range(\freqIndexOffset.kr(0), \freqIndexOffset.kr(0)+1).round(1).asInteger;
	var freq = Lag.kr(Select.kr(dirIndex, ~mavilaScaleLow),1.5)*(1+ (multi*0.008));
	sig = 0;
	mainEnv = EnvGen.ar(Env.asr(mainAttack,1, mainRelease, curve: -1),\gate.kr(1), doneAction:2);
	fallbackEnv = EnvGen.ar(~fallBackEnv, doneAction:2);

	sig = SinOsc.ar(freq, 0) * Lag.kr(cc.cos.range(0.01,1));

	sig = sig * finalAmp * mainEnv * fallbackEnv;
	Out.ar(fxBus, sig*0.4);
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig);
	//Out.ar(0, sig);
}).add;



SynthDef(\sineDrone, {
	var sig, env, mainEnv, noize,  fallbackEnv;
	var finalAmp = 0.05;
	var fallbackAttack = 0.001;
	var fallbackDecay = 120;
	var fallbackRelease = 30;
	var mainAttack = 0.005;
	var mainRelease = 2;
	var grained;
	var cc = \cc.kr(0).linlin(0,127,0,2pi);
	var dcc = \dcc.kr(0).linlin(0,255,0,2pi);
	var impFreq = cc.cos.range(1,20);
	var fxBus =\fxBus.ir(0);
	var dirBus = In.kr(\dirBus.kr(0), 1);
	var trig = InTrig.kr(\trigBus.kr(0), 1);
	var multi = Trig.kr(trig, 0.1);
	//var freq = \freq.kr(100);

	var dirIndex = dirBus.linlin(0,127,0,2pi).cos.range(\freqIndexOffset.kr(0), \freqIndexOffset.kr(0)+3).round(1).asInteger;
	var freq = Lag.kr(Select.kr(dirIndex, ~mavilaScaleLow),1.5)*(1+ (multi*0.01));
	sig = 0;
	mainEnv = EnvGen.ar(Env.asr(mainAttack,1, mainRelease, curve: -1),\gate.kr(1), doneAction:2);
	fallbackEnv = EnvGen.ar(Env.new(levels: [0, 1, 1, 0], times: [fallbackAttack, fallbackDecay, fallbackRelease], curve: [1, 0, -2]), doneAction:2);

	sig = VarSaw.ar(freq, 0, dcc.cos.range(0,1));
	//sig = VarSaw.ar(freq, 0, 0.0);

	sig = LPF.ar(sig,
		Lag.kr(
		dcc.cos.exprange(
			\ccSpeed.kr(1).linexp(0.1, 10, 300, 100),
			\ccSpeed.kr(1).linexp(0.1, 10, 1000, 500))
		, 0.1)
	)+  LPF.ar(sig, EnvGen.ar(Env.perc(0.2,0.5), trig).linexp(0,1, 300, 2000));

	sig = sig * finalAmp * mainEnv * fallbackEnv;
	Out.ar(fxBus, sig*0.4);
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig);
	//Out.ar(0, sig);
}).add;


/************************************************************/
SynthDef(\grainDrone, {
	var sig, env, mainEnv, noize,  fallbackEnv;
	var finalAmp = 0.33;
	var fallbackAttack = 1;
	var fallbackDecay = 120;
	var fallbackRelease = 30;
	var mainAttack = 1;
	var mainRelease = 1;
	var grained;
	var cc = \cc.kr(0).linlin(0,127,0,2pi);
	var impFreq = cc.cos.range(1,20);
	var fxBus =\fxBus.ir(0);
	var dirBus = In.kr(\dirBus.kr(0), 1);
	//var freq = \freq.kr(100);
	var trig = InTrig.kr(\trigBus.kr(0), 1);
	var multi = Trig.kr(trig, 0.1);
	var dirIndex = dirBus.linlin(0,127,0,2pi).cos.range(\freqIndexOffset.kr(0), \freqIndexOffset.kr(0)+3).round(1).asInteger;
	var freq = Lag.kr(Select.kr(dirIndex, ~mavilaScaleLow))*(1- multi*0.9);
	var tfreq = Trig.kr(trig, 0.2)*Impulse.ar(20);

	freq = Lag.kr(dirBus.linlin(0,127,0,2pi).cos.range(0.9, 1.1)*freq);
	sig = 0;
	mainEnv = EnvGen.ar(Env.asr(mainAttack,1, mainRelease, curve: -1),\gate.kr(1), doneAction:2);

	fallbackEnv = EnvGen.ar(Env.new(levels: [0, 1, 1, 0], times: [fallbackAttack, fallbackDecay, fallbackRelease], curve: [1, 0, -2]), doneAction:2);
	grained = TGrains.ar(1,

		trigger: Trig.kr(trig, 0.2)*Impulse.ar(\ccSpeed.kr(1).linexp(0.1, 10, 20, 1)),
		bufnum: \buf.ir(0),
		rate: dirBus.linlin(0,127,0.7,1),
		centerPos: dirBus.linlin(0,127,0,1),
		dur: Trig.kr(trig, 0.2) + 0.1
	);
	sig = grained + CombL.ar(grained, delaytime:SinOsc.ar(\combFreq.kr(0.01)).range(1/10, 1/11),decaytime:3, mul:0.4);
	sig = Pluck.ar(sig, Trig.kr(trig, 0.2)*Impulse.ar(20), delaytime:freq.reciprocal);
	sig = LPF.ar(grained, Lag.kr((\cc.kr(0)%127).linlin(0,127,0,2pi).cos.exprange(\ccSpeed.kr(1).linexp(0.1, 10, 300, 100),\ccSpeed.kr(1).linexp(0.1, 10, 1000, 500)), 0.1));


	sig = sig*0.8*mainEnv*fallbackEnv;
	Out.ar(fxBus, sig*0.2);
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig);
}).add;

//uracil drone
SynthDef.new(\droneNoize, {
	var sig, env, mainEnv, noize,  fallbackEnv;
	var finalAmp = 0.05;
	var fallbackAttack = 1;
	var fallbackDecay = 120;
	var fallbackRelease = 30;
	var mainAttack = 1;
	var mainRelease = 1;
	var dirBus = In.kr(\dirBus.kr(0), 1);


	var cc = \cc.kr(0).linlin(0,127,0,2pi);
	var hfspeed = (\cc.kr(0) % 32).linlin(0,32,0,2pi);
	var dcc = \dcc.kr(0);
	var tcc = \tcc.kr(0);

	var freq = \freq.ir(100)*1;
	var ccSpeed = \ccSpeed.kr(8).linlin(0.1, 16, 0.01*freq, 2*freq);
	//var cosDirCC = (dirCC.linlin(0, 127, 0, 1)*2pi).cos;

	var time = \ccSpeed.kr(8).linlin(0.1, 10, 0.1, 10) ;
	var multiEnv = 4.collect({arg i; EnvGen.ar(
		Env.new(
			levels: [0, 0, 1, 0, 0],
			times:[(i*1/4*time)+time/(5-i), time/8,time/2, (3-i)*time * 1/4],
			curve:\sine
	), Impulse.ar(1/time))});

	//freq = Lag.kr(dirBus.linlin(0,127,0,2pi).cos.range(freq, freq*2) + ccSpeed.reciprocal, 1);///\freq.kr(100)*0.1;
	freq = Lag.kr(dirBus.linlin(0,127,0,2pi).cos.range(1, 7).round(1)*freq);

	sig = 0;

	mainEnv = EnvGen.ar(Env.asr(mainAttack,1, mainRelease, curve: -1),\gate.kr(1), doneAction:2);
	fallbackEnv = EnvGen.ar(Env.new(levels: [0, 1, 1, 0], times: [fallbackAttack, fallbackDecay, fallbackRelease], curve: [1, 0, -2]), doneAction:2);

	noize = WhiteNoise.ar(1);
	sig = noize;
	//sig =noize+ CombL.ar(noize, delaytime:freq.reciprocal, decaytime:time, mul:0.2);

	sig = HPF.ar(sig, hfspeed.sin.range(3000, 7000));
	sig = BPF.ar(sig, (hfspeed).sin.range(3000, 10000), 0.7);

	sig = HPF.ar(sig, 100);
	sig = sig * mainEnv * finalAmp;
	sig = sig * multiEnv;

	Out.ar(\out.kr(0), sig);
}).add;


SynthDef(\invkarp, { arg freq=200, pan=0, trig=1, delaytime=0.2, decaytime=1.3, fxBus;
	var sig, env, finalAmp = 0.05, minFreqMul = 1, maxFreqMul = 1.7;
	var dirBus = In.kr(\dirBus.kr(0), 1);
	var freqMul = dirBus.linlin(0,127, minFreqMul, maxFreqMul);
	var coef = dirBus.linlin(0, 127, 0, 2pi).cos.range(0.5, 2.5);
	var dynAmp = \dur.ir(0.01).linlin(0.1,4,1.2,0.4);
	sig = CombC.ar(WhiteNoise.ar(1),1,  (freq*freqMul).reciprocal, 0.6);

	env = EnvGen.ar(Env.new([0,0.2,0],[\dur.ir(0.01)*0.5, 0.01], 4), doneAction: 2);
	sig = sig + VarSaw.ar(freq*freqMul*7, width:0.5);
	sig = LPF.ar(sig, EnvGen.ar(Env.new([100,10000,100],[\dur.ir(0.01)*0.4, \dur.ir(0.01)*0.1], 4)));
	sig = Mix.ar(sig);
	sig = sig * \amp.ir(1)* env * dynAmp * finalAmp;
	Out.ar(fxBus, sig*\mulPluckSynths.ir(1));
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig*\mulPluckSynths.ir(1));
}).add;


SynthDef(\letterSynth, {
	var sig = PlayBuf.ar(1, \buf.ir(0), BufRateScale.kr(\buf.ir(0)) * Rand(0.9, 1.2), doneAction:2);
	var finalAmp = 0.01;
	sig = sig * finalAmp * \amp.ir(1);
	sig = HPF.ar(sig, 200);
	Out.ar(\fxBus.ir(0), sig);
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig);
}).add;

SynthDef(\karp, { arg freq=200, fxBus, modBus;
	var sig, env, finalAmp = 0.05, minFreqMul = 1, maxFreqMul = 1.7;
	var mod = In.kr(modBus).linlin(0,384, 0, 2pi).cos.range(-0.005*freq, 0.005*freq);
	var dirBus = In.kr(\dirBus.ir(0));
	var coef = dirBus.linlin(0, 127, 0, 2pi).cos.range(0.05, 0.8);
	var freqMul = dirBus.linlin(0,127, minFreqMul, maxFreqMul);
	sig = Pluck.ar(WhiteNoise.ar(1), 1, 0.01, (freq*freqMul + mod).reciprocal, \dur.ir(1), coef);
	env = EnvGen.ar(Env.perc(0.01, \dur.ir(1)), doneAction: 2);
	sig = sig + SinOsc.ar(freq*freqMul + mod);
	sig = Mix.ar(sig);
	sig = sig * \amp.ir(1) * env * finalAmp;

	Out.ar(fxBus, sig*\mulPluckSynths.ir(1));
	Out.ar(\out.ir(0) + \ccIndex.ir(0), sig * \mulPluckSynths.ir(1));
}).add;


SynthDef(\slideSynth, {
	arg fxBus;
	var sig, env, finalAmp = 0.6;

	env = EnvGen.ar(Env.perc(0.01, \dur.ir(1)*0.1), doneAction: 2);
	/* test audio signal */
	sig = SinOsc.ar(env.linexp(0,1,1,\freq.ir(0)*0.1), mul:1.5) + Ringz.ar(WhiteNoise.ar(0.02), env.linexp(0,1,50,10000));
	sig = sig * env;
	//sig = SinOsc.ar(Env([freq * 10, freq*2, -4], [1, 0.1]).ar);
	//sig = sig * Env.perc(0.03, 0.1).ar(Done.freeSelf);
	sig = (sig*2).tanh;
	sig = sig*0.2;
	sig = Mix.ar(sig)*finalAmp * \amp.kr(1);
	Out.ar(fxBus, sig * 0.5*\mulPluckSynths.ir(1));
	Out.ar(\out.kr(0) + \ccIndex.kr(0), sig*\mulPluckSynths.ir(1));
}).add;

SynthDef(\noizeBurst, {
	arg fxBus;
	var sig, env, finalAmp = 0.5;
	var dirBus = In.kr(\dirBus.kr(0), 1);
	var dirIndex = dirBus.linlin(0,127,0,2pi).cos.range(20, 30).round(1).asInteger;
	var freq = Lag.kr(Select.kr(dirIndex, ~mavilaScaleLow)) * 2;


	env = EnvGen.ar(Env.perc(0.01, \dur.ir(1)*0.8), doneAction: 2);
	/* test audio signal */
	//sig = Ringz.ar(WhiteNoise.ar(1), env.linexp(0,1,50,10000)) +
	sig = SinOsc.ar(freq);
	sig = sig * env;
	//sig = SinOsc.ar(Env([freq * 10, freq*2, -4], [1, 0.1]).ar);
	//sig = sig * Env.perc(0.03, 0.1).ar(Done.freeSelf);
	sig = (sig*2).tanh;
	sig = sig*0.2;
	sig = Mix.ar(sig)*finalAmp * \amp.kr(1);
	Out.ar(fxBus, sig * 0.5*\mulPluckSynths.ir(1));
	Out.ar(\out.kr(0) + \ccIndex.kr(0), sig*\mulPluckSynths.ir(1));
}).add;


//Synth(\slideSynth, [\freq, 200]);

SynthDef.new(\vent, {
	var sig, env, rezo, comb, comb2, mainEnv, noize, rezo2, combVolume, noizeVolume, combEnv, fallbackEnv;
	var finalAmp = 0.04;
	var fallbackAttack = 1;
	var fallbackDecay = 120;
	var fallbackRelease = 30;
	var mainAttack = 10;
	var mainRelease = 10;

	var freq = \freq.kr(100);
	var fMin = (freq - (0.001 * freq)).reciprocal;
	var fMax = (freq + (0.001 * freq)).reciprocal;
	var cc = \cc.kr(0).linlin(0,127,0,2pi);
	var dcc = \dcc.kr(0);
	var tcc = \tcc.kr(0);
	var halfcc = cc % 64;
	var baseFreq = \noizeOffset.ir(0)*freq + 300; // !!! + freq? ma byt krat
	var dirCC = \dirCC.kr(0, 0.1);
	var ccSpeed = \ccSpeed.kr(0, 1).linlin(0.1, 16, 200, 500);
	var cosDirCC = (dirCC.linlin(0, 127, 0, 1)*2pi).cos;

	var freqCombChanges = 1 / tcc.linlin(0,384,2,20); // freqComb poskoci kazdych cca 2 az 20s
	var freqComb = Lag.kr( freq *
		(LFNoise0.kr(cosDirCC.range(0.2,5), mul: LFPulse.kr(freqCombChanges, 0, 0.1)).range(1,7)).round(1)
		+ cosDirCC.range(-0.05*freq, 0.05*freq), 0.1) * 0.25;
	var finalLPF = cosDirCC.linlin(-1,1,300, 10000);

	sig = 0;
	noizeVolume = 0.7;
	combVolume = 0.2; //0.7;
	mainEnv = EnvGen.ar(Env.asr(mainAttack,1, mainRelease, curve: -1),\gate.kr(1), doneAction:2);
	fallbackEnv = EnvGen.ar(Env.new(levels: [0, 1, 1, 0], times: [fallbackAttack, fallbackDecay, fallbackRelease], curve: [1, 0, -2]), doneAction:2);

	noize = WhiteNoise.ar();

	rezo = SinOsc.ar(0, cc, mul: dcc.linlin(0,255,50, 100)).lag;
	rezo2 = SinOsc.ar(0, tcc.linlin(0,384,0,2pi), mul:\mul.kr(50)).lag;

	comb = SinOsc.ar(0, dirCC.linlin(0,127,0,2pi)).linlin(-1,1, SinOsc.ar(0, dirCC.linlin(0,127,0,2pi)).range(fMin, fMax),0.02).lag;
	comb2 = SinOsc.ar(0, pi + cc).range(0.9, 10);
	combEnv = SinOsc.ar(0, tcc.linlin(0,384,0,2pi)).range(0.7, 1);


	//noize = BPF.ar(noize, baseFreq + ccSpeed + rezo, cosDirCC.range(0.3, 0.05));
	sig = CombC.ar(noize, 0.1, freqComb.reciprocal, comb2)*combEnv;

	sig = sig * WhiteNoise.ar(mul:0.1) + sig * 0.9;
	sig = LPF.ar(sig, SinOsc.ar(0, cc).range(1500, 15000));
	sig = HPF.ar(sig, (freq*2)  + ccSpeed + rezo2);

	sig = (sig + (sig * (noize*0.5))) * combVolume + (noize * noizeVolume);

	sig = LPF.ar(sig, finalLPF * SinOsc.ar(tcc.linlin(0,384,0.01, 0.1)).range(0.2, 1.2));
	sig = HPF.ar(sig, 100);

	sig = Mix.ar(sig);
	sig = Limiter.ar(sig);
	sig = sig * mainEnv * fallbackEnv;
	sig = sig * ~ventSynthsVol * finalAmp;
	Out.ar(\out.kr(0) + \ccIndex.kr(0), sig);
}).add;


~mavilaScaleLow = [
 149.433708,
152.133366,
154.881796,
164.235048,
167.202106,
170.222767,
183.763397,
187.083253,
201.965077,
205.613763,
 209.328366,
 221.969625,
 225.979712,
 230.062244,
 248.362898,
 252.849804,
 272.963128,
 277.89446,
 282.914881,
 300,
 305.419778,
	310.937469,
 329.714862,
 335.671466,
 341.735682,
 368.91957,
 375.584444,
 405.460883,
 412.78591,
 420.24327,
 445.621596,
 453.672163,
 461.868171,
 498.608182,
 507.616001,
 547.995092,
 557.895131,
 567.974024,
] * 0.5;

)



