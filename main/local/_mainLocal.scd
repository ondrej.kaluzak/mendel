(

s.options.numOutputBusChannels=2;
s.options.memSize = 8192 * 32;
s.options.blockSize = 2048;
o = Server.default.options;
o.outDevice_("ASIO : Komplete Audio 6");
MIDIClient.init;
MIDIIn.connectAll;

s.boot;
)

s.plotTree(0.1);
s = Server.quitAll

s.free
s.reboot;
s.quit
s.freeAll

(
var projectFolder = PathName(thisProcess.nowExecutingPath).parentPath;
var simulatorPath = projectFolder ++ '../sc_simulator/simulator.scd';
var mainPath = projectFolder ++ '../main.scd';


/****************************/
~audioChannelOffset = 0;
~ventCC = [1, 2, 3, 4, 5];
~ventCCMidiChan = [1, 1, 1, 1, 1] - 1;
~dirCC = [10, 11, 12, 13, 14];
~dirCCMidiChan = [1, 1, 1, 1, 1] - 1;
~ledDigiOutIndex = [1,3,5,9];
~ledDigiOutIndex = [0,1,0,1];
/****************************/

~masterVolumeRemote = 1;
~ledDurMultiplier = 0.7;

~masterVolAtten = 0.3;
~numOfSpeakers = 2;
~presetTimeMultiplier = 60; // resp.kolko sekund trva 1 minuta?
~presetRestMultiplier = 3;
~presetSynthMultiplier = 2;
~presetSwitcherTimeMultiplier = 1;

~localTimeOffset = 0;



~mendelONHours = (0..20);
~mendelON = true;


~ledSynthOn = true;


~maxAllowedActiveSynths = 30;
~minRootFreq = 100;


// od kolkych otacok za sekundu sa switchne na RPM mod
~rpmSwitch = -1; //momentalne deaktivovane
~nightHours = [99999] + ~localTimeOffset;
~forceNight = false;
//~mendelOFFHours = (10..14)++(25..29) ++ (40..44) ++ (55..59);
//~simulatorMeasureFreq = 70; //ms
//simulatorPath.load;
mainPath.load;

//Synth(\simulatorSynthOSC, ~simulatorSynthsGroup);

)

~trigBus[0].scope