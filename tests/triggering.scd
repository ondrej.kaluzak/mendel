(
var ccCount = 5;
var ventCC = [1,2,3,4, 5];
var numSpeakers = 2;
var ccMidiChan = 0;

var ccStartSynthTaskInterval = 6; //10
var ccTriggerSynthTaskInterval = [1,2,3,4, 5]; //10
var ccStopSynthTaskInterval = 1; // 0.1
var maxTime = 5; //5


var projectFolder = PathName(thisProcess.nowExecutingPath).parentPath;
var bazy = ["c","t","g","a","u"];
~letterBuffs = bazy.collect({arg letter;
		Buffer.read(s, projectFolder++"audio/" ++letter++ ".wav");
	});


~ventSynthOn = true;
~reverbSynth = Synth(\reverb, [\in, ~fxBus]);
~systemIsActive = ccCount.collect({false});

~ccSlowTimeSnapshot = ccCount.collect({0});
~slowSynthIsActive = ccCount.collect({false}); //~synth2Active = false;
~ventSynth = ccCount.collect({nil}); //~synth2 = nil;
~ventSynthsGroup = Group.new;







~ccTriggerSynthTask = ccCount.collect({
	arg i; var msg;
	Routine.new({
		inf.do{
			if(~ventSynthOn == true, {

				if(((~slowSynthIsActive[i] == false) && (~systemIsActive[i] == true)), {
					msg = s.makeBundle(false, {
						var freq = ~mavilaScaleLow.choose * (2.pow(i));
						~ventSynth[i] = Synth(\vent, [
							\out, i % numSpeakers,
							\noizeOffset, i,
							\freq, freq
							], ~ventSynthsGroup);

						~ventSynth[i].onFree({~ventSynth[i] = nil; ~slowSynthIsActive[i] = false;});
						NodeWatcher.register(~ventSynth[i]);
					});
					s.listSendBundle(nil, msg);
					~slowSynthIsActive[i] = true;

					[i, '--->activating<---'].postln;

				});
			});
			wait(ccTriggerSynthTaskInterval[i]);
		}
	});
});


~ccStopSynthTask = ccCount.collect({
	arg i;
	Routine.new({
		inf.do{
			var time = Date.getDate.rawSeconds;
			if((~ccSlowTimeSnapshot[i] - time).abs > maxTime, {
				if((~ventSynth[i] != nil) && (~slowSynthIsActive[i] == true),{
					~ventSynth[i].set(\gate, 0);
					~slowSynthIsActive[i] == false;
					~ventSynth[i] = nil;
					[i, '*****DEACTIVATING**'].postln;
				});
				~systemIsActive[i] = false;
			});
		wait(ccStopSynthTaskInterval);
		}
	});
});


~midiCC = ccCount.do({ arg ccIndex;
	var numOfTrigsInRound = 3;
	var numMeasurePoints = 4;
	var snpOffset = 20;
	var ccNum = ventCC[ccIndex];
	var timeLocked = false;
	var ccSnapshot = 0;
	var minRetrigTime = 80/1000;
	var waitingForTwin = numOfTrigsInRound.collect({false});
	var baseToneIndex = 0;
	var chordStep = 5;
	var mainRoundsCounter = 0;
	var maxRounds = 20;
	var mainSpeed = 3;

	var trigPos = numOfTrigsInRound.collect({arg i;
		numMeasurePoints.collect({arg point;
			(snpOffset + (42*i) + point).floor % 128;
		});
	});

	var twinPos = trigPos.collect({arg positions; (positions + 64) % 128});


	["trigPosition", trigPos].postln;
	["twinPosition", twinPos].postln;
	["waitingForTwin", waitingForTwin].postln;

	MIDIFunc.cc({
		arg ...args;

		var cc = args[0];
		var ccOffset = (cc + (127 - snpOffset))%128 ;
		var dcc = ccOffset + (128 * (mainRoundsCounter % 2));
		var tcc = ccOffset + (128 * (mainRoundsCounter % 3));

		var time = Date.getDate.rawSeconds;
		var pluckTrig = numOfTrigsInRound.collect({false});


		if((time - ccSnapshot).abs > minRetrigTime,{
			timeLocked = false;
		});

		numOfTrigsInRound.do({arg i;
			if(twinPos[i].includes(cc),{
				waitingForTwin[i] = false;
			});
		});

		if(timeLocked == false, {
			numOfTrigsInRound.do({arg i;
				if((trigPos[i].includes(cc) == true) && ( waitingForTwin[i] == false), {
					// do some stuff only for main trig possition
					if(i == 0, {

						// decide if system is active overall
						if((~ccSlowTimeSnapshot[ccIndex] - time).abs < maxTime, {
							~systemIsActive[ccIndex] = true;
						});

						mainSpeed = time-ccSnapshot;
						baseToneIndex = ~mavilaScaleLow.size.rand;
						mainRoundsCounter = (mainRoundsCounter + 1) % maxRounds;
					});

					~ccSlowTimeSnapshot[ccIndex] = time;

					ccSnapshot = time;
					timeLocked = true;
					pluckTrig[i] = true;
					waitingForTwin[i] = true;
				});
			});
		});

		// [mainRoundsCounter, cc, dcc, tcc].postln;

		numOfTrigsInRound.do({arg i;
			if((pluckTrig[i] == true),{
				var freq = ~mavilaScaleLow.foldAt(baseToneIndex + (i*chordStep)) * (2.pow(ccIndex));
				var dur = mainSpeed.lincurve(0.1, 5, 0.1, 16, 4);
				var room = mainSpeed.lincurve(0.1, 10, 0.3, 2, -2);
				~reverbSynth.set(\room, room);

				if((mainRoundsCounter == 5) &&(~ventSynth[ccIndex]!= nil) && (i == 0), {
					~ventSynth[ccIndex].set(\freq, freq);
				});

				if(mainRoundsCounter > (maxRounds - (maxRounds / 5)),{
					Synth(\letterSynth, [
						\buf,~letterBuffs[ccIndex],
						\out, ccIndex % numSpeakers,
						\fxBus, ~fxBus
					]);
				});

				if((mainRoundsCounter > 5) && (mainRoundsCounter < 15),{
					Synth(\invkarp, [
						\freq, freq,
						\out, ccIndex % numSpeakers,
						\dur, dur,
						\fxBus, ~fxBus,
						\modBus, ~modBus
					]);
				});

				if(mainRoundsCounter <= 10,{
					Synth(\karp, [
						\freq, freq,
						\out, ccIndex % numSpeakers,
						\dur, dur,
						\fxBus, ~fxBus,
						\modBus, ~modBus
					]);
				});

			});
		});
		if(timeLocked == false && ccIndex == 0, {~modBus.set(tcc)});

		if(~ventSynth[ccIndex]!= nil, {
			~ventSynth[ccIndex].set(\cc, cc);
			~ventSynth[ccIndex].set(\dcc, dcc );
			~ventSynth[ccIndex].set(\tcc, tcc );
			~ventSynth[ccIndex].set(\ccSpeed, mainSpeed );
		});

	},
	ccNum, ccMidiChan);
});

~ccTriggerSynthTask.do({arg t; t.play});
~ccStopSynthTask.do({arg t; t.play});
)

(
)


/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/**********************single*******************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/



(

var numOfTrigsInRound = 3;
var numMeasurePoints = 4;
var snpOffset = 20;
var ccNum = 1;
var ccMidiChan = 0;
var prevCC = 64;
var timeLocked = false;
var ccRound = 0;
var ccSnapshot = 0;
var minRetrigTime = 40/1000;
var waitingForTwin = numOfTrigsInRound.collect({false});
var baseToneIndex = 0;
var chordStep = 5;

var trigPos = numOfTrigsInRound.collect({arg i;
		numMeasurePoints.collect({arg point;
		(snpOffset + (42*i) + point).floor % 128;
		});
});

var twinPos = trigPos.collect({arg positions; (positions + 64) % 128});

["trigPosition", trigPos].postln;
["twinPosition", twinPos].postln;
["waitingForTwin", waitingForTwin].postln;

MIDIFunc.cc({
	arg ...args;

	var cc = args[0];
	var time = Date.getDate.rawSeconds;
	var pluckTrig = numOfTrigsInRound.collect({false});


	if((time - ccSnapshot).abs > minRetrigTime,{
		timeLocked = false;
	});

	numOfTrigsInRound.do({arg i;
		if(twinPos[i].includes(cc),{
			waitingForTwin[i] = false;
		});
	});

	if(timeLocked == false, {
		numOfTrigsInRound.do({arg i;
			if((trigPos[i].includes(cc) == true) && ( waitingForTwin[i] == false), {
				ccSnapshot = time;
				timeLocked = true;
				pluckTrig[i] = true;
				waitingForTwin[i] = true;
				if(i == 0, { baseToneIndex = ~mavilaScaleLow.size.rand });
				ccRound = (ccRound + 1) % 4;
				["ccRound, cc, mcc", ccRound, cc, cc+128*ccRound].postln;
			});
		});
	});
	numOfTrigsInRound.do({arg i;
		if((pluckTrig[i] == true),{
			Synth(\karp, [\freq, ~mavilaScaleLow.foldAt(baseToneIndex + (i*chordStep))]);

		});
	});

	}, ccNum, ccMidiChan);
)


/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/



s.boot;
s.plotTree
MIDIClient.init;
MIDIIn.connectAll;

(
var numHotPos = 3;
var numMeasurePoints = 5;
var snpOffset = 37;
var ccNum = 1;
var ccMidiChan = 0;
var prevCC = 64;
var pluckLocked = false;
var ccRound = 0;
var ccSnapshot = 0;
var minRetrigTime = 0.05;
var baseTrigPosition = [0,1,2,3,4] + snpOffset;
var trigPosition = [0,1,2,3,4] + snpOffset;
var waitingForTwin = false;
var twinPosition = trigPosition + 64;
trigPosition = trigPosition ++ (trigPosition + 40) ++ (trigPosition + 80);


/*
var trigPositions = numHotPos.collect({arg i;
		numMeasurePoints.collect({arg point;
			((snpOffset)*(i+1) + point).floor % 128;
		});
});

var twinPosition = numHotPos.collect({arg i;
		numMeasurePoints.collect({arg point;
			((snpOffset)*(i+1) + point).floor % 128;
		});
});

["trigPosition", trigPositions].postln;

)
*/
["twinPosition", twinPosition].postln;
//["trigPosition", trigPosition].postln;


MIDIFunc.cc({
	arg ...args;
	var cc = args[0], time, pluckTrig = false;
	time = Date.getDate.rawSeconds;

	if((time - ccSnapshot).abs > minRetrigTime,{
		pluckLocked = false;
	});

	if(twinPosition.includes(cc),{
		waitingForTwin = false;
	});

	if( (trigPosition.includes(cc) == true) && (pluckLocked == false) && ( waitingForTwin == false), {
		ccSnapshot = time;
		pluckLocked = true;
		pluckTrig = true;
		waitingForTwin = true;

		ccRound = (ccRound + 1) % 4;
		["ccRound, cc, mcc", ccRound, cc, cc+128*ccRound].postln;
	});
	prevCC = cc;

	if((pluckTrig == true),{
		Synth(\karp, [\freq, ~mavilaScale.choose]);
	});

	},
ccNum, ccMidiChan);
)
