

(

~fxBus = Bus.audio(s, 1);
~modBus = 5.collect({Bus.control(s, 1)});
~dirBus = 5.collect({Bus.control(s, 1)});
~trigBus = 5.collect({Bus.control(s, 1)});
~masterBus = Bus.audio(s,4);
~audioChannelsIndexes = [0,1,2,3];
~reverbVolume = 0.58;
~masterVolume = 0.99;
~ventSynthsVol = 0.0;
~audioChannelOffset = 0;


SynthDef(\reverb, {
	var in = In.ar(\in.kr(0));
	var sig = FreeVerb.ar(in, 1, \room.kr(0.5), mul: \vol.kr(0.1));
	sig = LPF.ar(sig, 8000);

	sig = HPF.ar(sig, 200);
	sig = LPF.ar(sig, 5000);

	sig = sig!4;
	Out.ar(~masterBus , sig);
}).add;

SynthDef(\master, {
	var sig = In.ar(\in.kr(0), 4);
	sig = sig * \vol.kr(0.5, 100);
	sig = Compander.ar(sig*1.3, sig, 0.3);

	sig = LPF.ar(sig, \nightLPF.kr(s.sampleRate / 2, 100));

	sig = Limiter.ar(sig);
	Out.ar(~audioChannelOffset, sig);
}).add;

)


/*****************************/


~reverbSynth.set(\vol, 0.0);
~masterSynth.set(\vol, 0.99);

s.freeAll;
s.quit
s.boot
s.plotTree
s.freeAll


(
var time = 16;

var env = 4.collect({arg i; Env.new(levels: [0, 0, 1, 0, 0], times:[(i*1/4*time), time/8,time/8, (3-i)*time * 1/4], curve:\sine)});
env = EnvGen.ar(Env.perc([0,1]), 1);
env;

//4.collect({arg i; [(i*1/4*time), time/8,time/8, (3-i)*time * 1/4]});
//4.collect({arg i; Env.new(levels: [0, 1, 0, 0], times:[time/8,time/8, time * 3/4])}).plot;
//4.collect({arg i; Env.new(levels: [0, 1, 0, 0], times:[2,2,2])}).plot;
 //time * 3/4
)